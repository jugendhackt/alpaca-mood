const express = require('express');
const bodyParser = require('body-parser')
const path = require('path');
const app = express();

app.use(express.static(path.join(__dirname, '/../build')));

const allVotes = [];
for (let i=1; i<=8; ++i) {
    allVotes.push({key: i, votes: 0});
}

app.get('/allVotes', function (req, res) {
 return res.send({allVotes});
});

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, '/../build', 'index.html'));
});

app.listen(process.env.PORT || 8080);