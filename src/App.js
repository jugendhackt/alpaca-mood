import React, {useState, useEffect} from 'react';
import { AlpacaTile } from './AlpacaTile';

function App() {
  const [isLoading, setIsLoading] = useState(true);
  const [votes, setVotes] = useState(null);
  const [totalVotes, setTotalVotes] = useState(0);

  useEffect(() => {
    // TODO use websocket for streaming
    fetch('/allVotes')
      .then(res => res.json())
      .then((result) => {
        console.error(result)
        const { allVotes } = result;
        setVotes(allVotes)
        setIsLoading(false);
        setTotalVotes(allVotes.map((el) => el.votes).reduce((acc, curr) => acc + curr));
      });
    
  }, []); // empty deps so only once

  if (isLoading) {
    return <div className="content">fetching data from server</div>
  }

  return (
    <div className="content">
    <h1>Alpaca Mood Votes</h1>
    <h2>All votes: {totalVotes}</h2>
    <div className="tileWrapper">
      {votes.map((el) => <AlpacaTile key={el.key} alpacaType={el.key} id={`alpaca-mood-tile-${el.key}`} votes={el.votes} totalVotes={totalVotes} />)}
    </div>
    <footer>Graphics <a href="https://creativecommons.org/licenses/by/4.0/deed.de" rel="noreferrer" target="_blank">CC-BY 4.0</a> Michelle Posmyk, Website Build by <a href="https://gitlab.com/yuki-93" target="_blank" rel="noreferrer">Yuki</a> for Jugendhackt</footer>
    </div>
  );
}

export default App;