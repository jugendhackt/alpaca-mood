import React from 'react';

export const AlpacaTile = ({alpacaType, votes, totalVotes}) => {
    const foo= 'bar';
    return (
        <div className='alpacaTile'>
            <img src={`/assets/Alpaka ${alpacaType}.2S.png`} alt={`mood alpaca number ${alpacaType}`}/>
            <div>{`${votes} / ${totalVotes}`}</div>
        </div>
    )
}